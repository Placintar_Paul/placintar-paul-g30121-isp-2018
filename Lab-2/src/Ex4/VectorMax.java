package Ex4;

import java.util.Random;

public class VectorMax {

    static int Max(int[] n)
    {
        int max = 0;
        for(int i = 0; i< n.length; i++)
        {
            if(n[i]>max) max= n[i];
        }
        return max;
    }

    static int[] CreateVector(int size)
    {
        int[] vector = new int[size];
        for(int i=0; i<vector.length; i++)
        {
            vector[i] = new Random().nextInt(50);
            System.out.println(vector[i]);
        }
        return vector;
    }
    public static void main(String[] args) {

        int size = 10;
        System.out.println("Max from vector is: "+Max(CreateVector(size)));
    }
}
