package Ex6;

public class Factorial {

    static void NonRecursive(int n)
    {
        int sum=1;

        if(n ==0 || n==1) System.out.println("NonRecursive method: "+sum);

        else
            {
                for (int i = 1; i <= n; i++)
                {
                    sum *= i;
                }
            System.out.println("NonRecursive method: " + sum);
        }
    }

    static int Recursive(int n)
    {
        int fact;

        if(n ==0 || n==1) return 1;

        else
        {
            return n*Recursive(n-1);
        }

    }
    public static void main(String[] args) {
        int number = 4;

        NonRecursive(number);
        System.out.println("Recursive method: "+Recursive(number));
    }
}
