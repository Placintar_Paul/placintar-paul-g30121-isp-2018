package Ex7;

import java.util.Random;
import java.util.Scanner;

public class Game {

    public static void main(String[] args) {
        int number = new Random().nextInt(10);
        System.out.println("Number assigned: "+number);
        int retries = 0;
        Scanner scan = new Scanner(System.in);
        int x;

        while (retries <= 2)
        {
            System.out.println("Guess the number: ");
            x = scan.nextInt();

            if (x>number)
            {
                System.out.println("Too big, try again");
                retries++;
            }
            else if (x<number)
            {
                System.out.println("Too small, try again");
                retries++;
            }
            else
            {
                System.out.println("Congrats, you WON!");
                break;
            }

        }

        if(retries > 2) System.out.println("Sorry, you lost :(");

    }
}
