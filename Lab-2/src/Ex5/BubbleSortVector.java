package Ex5;


import java.util.Random;

public class BubbleSortVector {

    static int[] CreateVector(int size)
    {
        int[] vector = new int[size];
        for(int i=0; i < vector.length; i++)
        {
            vector[i] = new Random().nextInt(50);
        }
        return vector;
    }

    static int[] BubbleSort (int[] n)
    {

        boolean NotSorted = false;
        do {
            NotSorted = false;

            for (int i = 0; i < n.length-1; i++)
            {
                if(n[i]>n[i+1])
                {
                    int temp = n[i];
                    n[i] = n[i + 1];
                    n[i + 1] = temp;
                    NotSorted = true;
                }
            }

        }while(NotSorted);

        return n;
    }

    static void PrintVector (int[] n)
    {
        for (int i = 0; i < n.length ; i++)
        {
            System.out.println(n[i]);
        }
    }

    public static void main(String[] args) {

        int vectorSize = 5;
        int[] n = CreateVector(vectorSize);
        PrintVector(n);
        System.out.println("Check");
        PrintVector(BubbleSort(n));
    }
}
