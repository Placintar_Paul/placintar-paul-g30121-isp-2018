package Ex3;

import java.util.Scanner;

public class PrimeNumbers {

    static void prime(int a, int b)
    {
        int counter = 0;
        for (int i = a; i <= b; i++)
        {
            boolean prime = true;

            for(int j=1; j < i; j++)
            {
                if(i%j==0 && j!= 1)
                {
                    prime = false;
                    break;
                }
            }
            if (prime)
            {
                System.out.println(i);
                counter +=1;
            }
        }
        System.out.println("Prime numbers found: "+counter);

    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        prime(a,b);

    }
}
