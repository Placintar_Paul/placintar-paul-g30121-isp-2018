package Ex1;

public class Sensor {


    private int value;

    Sensor()
    {
        this.value = -1;
    }

    public void add(int n)
    {
        value += n;
    }

    public int get()
    {
        return value;
    }


}



