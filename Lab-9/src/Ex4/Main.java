package Ex4;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Game");
        GameForm game = new GameForm();
        frame.setContentPane(game.MainBoard);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        game.initGame();
        game.gameController();


    }
}
