package Ex4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

public class GameForm {

    public JPanel MainBoard;


    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JButton button9;
    private JTextField textField1;



    private static boolean gameWon;
    private static boolean playerTurn;
    private static int currentPosition;
    private static String currentMark;
    private static int movesCounter;


    public void initGame()
    {
        button1.setText("");
        button2.setText("");
        button3.setText("");
        button4.setText("");
        button5.setText("");
        button6.setText("");
        button7.setText("");
        button8.setText("");
        button9.setText("");

        currentPosition = 10;
        gameWon = false;
        playerTurn = false;
        currentMark = "X";
        movesCounter = 0;

    }


    private boolean draw()
    {
        return!(button1.getText().equals("") &&
                button2.getText().equals("") &&
                button3.getText().equals("") &&
                button4.getText().equals("") &&
                button5.getText().equals("") &&
                button6.getText().equals("") &&
                button7.getText().equals("") &&
                button8.getText().equals("") &&
                button9.getText().equals("") );
    }


    public static void turnCheck()
    {
        playerTurn = !playerTurn;
        if (currentMark.equals("X")) currentMark = "O";
        else currentMark="X";

    }

    public void gameController()
    {
        gameWon = checkWinCondition(currentPosition);
        movesCounter++;
        if(gameWon)
        {
            if(draw())
                textField1.setText("Player" + currentMark + " Won !"+"   Next game!");
            else
            textField1.setText("Player" + currentMark + " Won !"+"   Next game!");
        }
        else{
            turnCheck();
            textField1.setText("It's " + currentMark + " turn");
        }
        System.out.println(gameWon);

    }


    public boolean checkWinCondition(int currentPosition)
    {

        if(    button1.getText().equals(currentMark)&&
                button2.getText().equals(currentMark)&&
                button3.getText().equals(currentMark))
            return true;
        else if (button4.getText().equals(currentMark)&&
                button5.getText().equals(currentMark)&&
                button6.getText().equals(currentMark))
            return true;
        else if (button7.getText().equals(currentMark)&&
                button8.getText().equals(currentMark)&&
                button9.getText().equals(currentMark))
            return true;
        else if (button1.getText().equals(currentMark)&&
                button4.getText().equals(currentMark)&&
                button7.getText().equals(currentMark))
            return true;
        else if (button2.getText().equals(currentMark)&&
                button5.getText().equals(currentMark)&&
                button8.getText().equals(currentMark))
            return true;
        else if (button3.getText().equals(currentMark)&&
                button6.getText().equals(currentMark)&&
                button9.getText().equals(currentMark))
            return true;
        else if (button1.getText().equals(currentMark)&&
                 button5.getText().equals(currentMark)&&
                 button9.getText().equals(currentMark))
            return true;
        else if (button3.getText().equals(currentMark)&&
                button5.getText().equals(currentMark)&&
                button7.getText().equals(currentMark))
            return true;

        return false;
    }

    public GameForm() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button1.setText(currentMark);
                currentPosition = 0;
                gameController();

            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button2.setText(currentMark);
                currentPosition = 0;
                gameController();

            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button3.setText(currentMark);
                currentPosition = 0;
                gameController();

            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button4.setText(currentMark);
                currentPosition = 1;
                gameController();

            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button5.setText(currentMark);
                currentPosition = 1;
                gameController();

            }
        });
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button6.setText(currentMark);
                currentPosition = 1;
                gameController();

            }
        });
        button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button7.setText(currentMark);
                currentPosition = 2;
                gameController();

            }
        });
        button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button8.setText(currentMark);
                currentPosition = 2;
                gameController();

            }
        });
        button9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button9.setText(currentMark);
                currentPosition = 2;
                gameController();

            }
        });


    }



    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
