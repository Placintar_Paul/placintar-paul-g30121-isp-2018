package Ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class  Counter  extends JFrame {


    JTextField t1;
    JButton b1;
    JLabel l1;

    Counter()
    {
        setTitle("Test Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,250);
        setVisible(true);
    }

    public void init()
    {
        this.setLayout(null);
        int width=160;int height = 60;

        l1 = new JLabel("Counter");
        l1.setBounds(10, 80,width, height);

        t1 = new JTextField("0");
        t1.setBounds(70, 100,width, height);
        t1.setSize(80,20);

        b1 = new JButton("Increment");
        b1.setBounds(70, 140,width, height);
        b1.setSize(100,20);
        b1.addActionListener(new A());

        add(l1);add(t1);add(b1);
    }



    public static void main(String[] args) {
        new Counter();
    }

    class A implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

             int n = Integer.parseInt(Counter.this.t1.getText());
             n++;
             Counter.this.t1.setText(String.valueOf(n));
        }
    }


}
