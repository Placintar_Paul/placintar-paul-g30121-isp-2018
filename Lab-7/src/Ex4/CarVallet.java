package Ex4;

import java.io.*;

public class CarVallet {


        CarObject createCar(String name,Integer price)
        {
            CarObject c = new CarObject(name,price);
            return c;
        }

        public void parkCar (CarObject car,String parkingSpot) throws IOException
        {
            ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(parkingSpot));
            o.writeObject(car);
            System.out.println("The car: " +car.getModel()+ " was parked here: "+parkingSpot);
        }

        public CarObject retriveCar(String parkingSpot) throws IOException, ClassNotFoundException {
            ObjectInputStream o = new ObjectInputStream(new FileInputStream(parkingSpot));

            CarObject car = (CarObject) o.readObject();
            System.out.println("The car: " +car.getModel()+ " is ready to drive");
            return car;
        }



}
