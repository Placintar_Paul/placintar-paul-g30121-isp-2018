package Ex4;

import java.io.IOException;

public class Test {
    public static void main(String[] args) throws Exception {
        CarVallet v = new CarVallet();

        CarObject c1 = v.createCar("BMW",1000);
        CarObject c2 = v.createCar("Dacia",10);

        v.parkCar(c1,"Car1.dat");
        v.parkCar(c2,"Car2.dat");

        CarObject x = v.retriveCar("Car1.dat");
        CarObject y = v.retriveCar("Car2.dat");

        System.out.println(x.toString());
        System.out.println(y.toString());

    }
}
