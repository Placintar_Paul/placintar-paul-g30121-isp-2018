package Ex4;

import java.io.Serializable;

public class CarObject implements Serializable {
    String model;
    Integer price;

    public String getModel() {
        return model;
    }

    @Override
    public String toString() {
        return "CarObject{" +
                "model='" + model + '\'' +
                ", price=" + price +
                '}';
    }

    public CarObject(String model, Integer price) {
        this.model = model;
        this.price = price;
    }
}
