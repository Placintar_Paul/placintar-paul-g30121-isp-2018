package Ex4;

import jdk.nashorn.internal.runtime.ECMAException;

public class AlarmUnit {

    protected String owner;

    public AlarmUnit() {
    }

    public AlarmUnit(String owner) {
        this.owner = owner;
    }

    public void event(Event event) throws Exception {

        if (event.getType().equals(EventType.FIRE) && ((FireEvent)event).isSmoke() == true)
        {
            GsmUnit g = new GsmUnit();
            g.callOwner();
            throw new Exception("Fire in the house");
        }

    }

}
