package Ex2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Test {

    public static void main(String[] args) {

        Bank bank = new Bank();
        bank.addAccount("Paul",100);
        bank.addAccount("Dan",300);
        bank.addAccount("Marc",500);
        bank.addAccount("Ion",200);
        bank.addAccount("Alex",10);

       // bank.printAccounts();
        // System.out.println("Second print");
        // bank.printAccounts(200,300);
        //System.out.println(bank.getAccount("Marc").getBalance());

        List accounts_2 = (List) bank.getAllAccounts()
                                 .stream()
                                 .sorted((b1,b2)->((BankAccount)b1).getOwner().compareTo(((BankAccount)b2).getOwner()))
                                 .collect(Collectors.toList());

        accounts_2.forEach(b->System.out.println("Owner: "+((BankAccount)b).getOwner()+" Balance: "+((BankAccount)b).getBalance()));

    }
}
