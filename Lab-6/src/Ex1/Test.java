package Ex1;

import java.util.Objects;

public class Test {

    public static void main(String[] args) {
        BankAccount b1 = new BankAccount();
        BankAccount b2 = new BankAccount();

        b1.deposit(1000);
        b2.deposit(1000);

        System.out.println(b1.equals(b2));
        System.out.println(b1.hashCode());

    }
}
