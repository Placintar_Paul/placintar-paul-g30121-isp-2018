package Ex1;

import java.util.Objects;

public class BankAccount {

    private String owner;
    private double balance;

    public void withdraw(double amount)
    {
        if (this.balance <= amount)
        {
            System.out.println("Insufficient founds");
        }
        else
            this.balance -=amount;
    }
    public void deposit(double amount)
    {
        this.balance +=amount;
    }

    @Override
    public boolean equals(Object obj) {

       if(obj == this) return true;
       if(!(obj instanceof BankAccount)) return false;

       return Objects.equals(this.balance, ((BankAccount)obj).balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }
}
