package Ex3;

import sun.reflect.generics.tree.Tree;

import java.util.*;
import java.util.stream.Collectors;

public class Test {



    public static void main(String[] args) {

        Bank bank = new Bank();
        bank.addAccount("Paul",100);
        bank.addAccount("Dan",300);
        bank.addAccount("Marc",500);
        bank.addAccount("Ion",200);
        bank.addAccount("Alex",10);

        bank.printAccounts();
        System.out.println("Second print");
        bank.printAccounts(200,300);
        System.out.println(bank.getAccount("Marc").getBalance());


        Comparator<BankAccount> customComp = new Comparator<BankAccount>() {
            @Override
            public int compare(BankAccount o1, BankAccount o2) {
                return o1.getOwner().compareTo(o2.getOwner());
            }
        };
        TreeSet t1 = new TreeSet(customComp);
        t1.addAll(bank.getAllAccounts());
        t1.forEach(b->System.out.println("Owner: "+((BankAccount)b).getOwner()+" Balance: "+ ((BankAccount)b).getBalance()));
    }
}
