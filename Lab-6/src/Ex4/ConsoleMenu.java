package Ex4;

import java.util.Scanner;

public class ConsoleMenu {
    public static void main(String[] args)  {

/*
        Dictionary dic = new Dictionary();
        Word w = new Word("Paul");
        Definition d = new Definition("Bani");
        dic.addWord(w,d);
        Word w2 = new Word("Alin");
        Definition d2 = new Definition("Masina");
        dic.addWord(w2,d2);
        Word w3 = new Word("Ana");
        Definition d3 = new Definition("Mere");
        dic.addWord(w3,d3);

        Word w_get = new Word("Alin");
        System.out.println("Custom get: " + dic.getDefinition(w_get).getDescription());
        dic.getAllDefinitions();
        dic.getAllWords();
*/
        System.out.println("Welcome!");



        Scanner scan = new Scanner(System.in);
        Dictionary d = new Dictionary();
        int choice = 0;


        while(choice <=4)
        {
            System.out.println("1.Add item \n2.Get value\n3.Get all keys\n4.Get all values \n5.Exit");
            System.out.println("Enter choice: ");
            choice = scan.nextInt();
            switch (choice)
            {
                case 1:
                {
                    System.out.println("Add word: \n");
                    Word w = new Word(scan.next());
                    Definition d1 = new Definition(scan.next());
                    d.addWord(w,d1);
                    break;
                }
                case 2:
                {
                    System.out.println("Input key for word: ");
                    Word w = new Word(scan.next());
                    System.out.println("Definition is: " + d.getDefinition(w).getDescription());
                    break;
                }
                case 3:
                {
                    System.out.println("Getting all words: ");
                    d.getAllWords();
                    break;
                }
                case 4:
                    System.out.println("Get all definitions: ");
                    d.getAllDefinitions();
                    break;
                default:
                    break;
            }
        }



    }
}
