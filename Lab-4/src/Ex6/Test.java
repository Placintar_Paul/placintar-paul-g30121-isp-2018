package Ex6;

public class Test {

    public static void main(String[] args) {
        Shape s = new Shape("blue", true);
        System.out.println(s.toString());

        Circle r = new Circle(2,"yellow",false);
        System.out.println(r.isFilled());

        Square q = new Square(2,"brown",true);
        q.setLenght(4);

        System.out.println(q.getSide());
    }
}