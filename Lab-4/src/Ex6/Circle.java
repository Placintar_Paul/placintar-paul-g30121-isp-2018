package Ex6;

public class Circle extends Shape {
    private double radius;

    public Circle()
    {
        this.radius =1.0;
    }

    public Circle (double radius)
    {
       this.radius = radius;
    }

    public Circle (double radius, String color, boolean filled)
    {
        super (color,filled);
        this.radius = radius;
    }

    public double getRadius()
    {
        return this.radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getAria()
    {
        return this.radius*this.radius*3.14;
    }

    public double getPerimeter()
    {
        return 2*3.14*this.radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + this.getRadius() +
                '}';
    }
}
