package Ex4;

import Ex2.Author;

import java.util.Arrays;

public class Book {

    private String name;
    private double price;
    private int qtyInStock = 0;
    private Author[] author;

    public Book (String name, Author[] author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }
    public Book (String name, Author[] author, double price, int qtyInStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName()
    {
        return this.name;
    }

    public Author[] getAuthor()
    {
        return this.author;
    }

    public double getPrice() {
        return this.price;
    }

    public int getQtyInStock() {
        return this.qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public void printAuthors()
    {

        for (Author a : this.author)
        {
            System.out.println(a.getName());
        }
    }

    @Override
    public String toString() {
        return "Book - "+ getName() +" by " + author.length + " auhors. ";
    }
}
