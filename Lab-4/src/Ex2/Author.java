package Ex2;

public class Author {

    private String name;
    private String email;
    private char gender;

    public Author(String name,String email, char gender)
    {
        if(Character.toLowerCase(gender)=='m' || Character.toLowerCase(gender)=='f' )
        {
            this.gender = gender;
        }else System.out.println("Gender not accepted for: "+name);

        this.name = name;
        this.email = email;
    }

    public String getName()
    {
        return this.name;
    }
    public String getEmail()
    {
        return this.email;
    }
    public char getGender()
    {
        return this.gender;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    @Override
    public String toString()
    {
        return "Author - " + this.name + "(" + this.gender + ")" + " at "+ this.email;
    }

}
