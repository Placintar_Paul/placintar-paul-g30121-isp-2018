package Ex3;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Controller
{

    public void control() throws InterruptedException {


        Sensor s = new TemperatureSensor();
        Sensor l = new LightSensor();

      //Prima methoda, dar am vrut sa nu am exceptii aruncate in cod
        int sec = 1;
        while (sec <=20)
        {
            System.out.println("Temp: " + s.readValue());
            System.out.println("Light: " + l.readValue());
            System.out.println("Sec: " + sec);
            sec++;
            Thread.sleep(1000);
        }

       /* final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(new Runnable()
        {
            int sec = 1;
            @Override
            public void run() {

                System.out.println("Temp: " + s.readValue());
                System.out.println("Light: " + l.readValue());
                System.out.println("Sec: " + sec);

                if(sec >= 20){ executorService.shutdown();}
                sec++;
            }
        }, 0, 1, TimeUnit.SECONDS);
        */
    }
}
