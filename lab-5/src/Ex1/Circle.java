package Ex1;


public class Circle extends Shape {
    protected double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {

    }
    public Circle(double radius, String color, boolean filled) {
        this.radius=radius;
        this.setColor(color);
        this.setFilled(filled);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getAria()
    {
        return 3.14*this.radius*this.radius;
    }

    public double getPerimeter()
    {
        return 3.14*2*this.radius;
    }

    @Override
    public String toString()
    {
        return "Circle";
    }

}
