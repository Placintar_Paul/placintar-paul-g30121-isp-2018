package Ex1;

abstract class Shape {
    protected String color;
    protected boolean filled;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    abstract double getAria();
    abstract double getPerimeter();

    @Override
    public String toString() {
        return "This is a shape";
    }
}
