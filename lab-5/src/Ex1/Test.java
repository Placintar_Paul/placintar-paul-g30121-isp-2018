package Ex1;

public class Test {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[3];
        shapes[0] = new Circle(3,"blue",true);
        shapes[1] = new Rectangle(1,4,"red",true);
        shapes[2] = new Square(4,"red",true);


            for (Shape s : shapes) {

                System.out.println(s.toString());
                System.out.println(s.getAria());
                System.out.println(s.getPerimeter());

            }

    }
}
