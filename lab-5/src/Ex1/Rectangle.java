package Ex1;

public class Rectangle extends Shape{
    protected double with;
    protected double lenght;

    public Rectangle(double with, double lenght) {
        this.with = with;
        this.lenght = lenght;
    }

    public Rectangle() {
    }
    public Rectangle(double with, double lenght,String color, boolean filled) {
        this.with = with;
        this.lenght = lenght;
        this.setColor(color);
        this.setFilled(filled);
    }

    public double getWith() {
        return with;
    }

    public void setWith(double with) {
        this.with = with;
    }

    public double getLenght() {
        return lenght;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    public double getAria()
    {
        return this.with*this.lenght;
    }

    public double getPerimeter()
    {
        return 2*with+2*lenght;
    }

    @Override
    public String toString() {
        return "Rectangle";
    }
}
