package Ex2;

public class Test {

    public static void main(String[] args) {
        ProxyImage p = new ProxyImage("Real");
        p.display();
        ProxyImage p1 = new ProxyImage("Rotated");
        p1.display();
    }
}
